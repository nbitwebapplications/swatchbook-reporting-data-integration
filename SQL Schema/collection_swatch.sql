﻿USE [SwatchbookData]
GO

/****** Object:  Table [dbo].[collection_swatch]    Script Date: 1/25/2021 2:07:19 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[collection_swatch](
	[id] [varchar](100) NOT NULL,
	[collection_id] [varchar](50) NOT NULL,
	[swatch_id] [varchar](50) NOT NULL,
	[job_update_at] [datetime] NOT NULL,
 CONSTRAINT [PK_collection_swatch] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


