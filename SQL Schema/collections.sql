﻿USE [SwatchbookData]
GO

/****** Object:  Table [dbo].[collections]    Script Date: 2/3/2021 12:09:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[collections](
	[id] [varchar](50) NOT NULL,
	[name] [varchar](100) NOT NULL,
	[updated_at] [datetime] NOT NULL,
	[job_update_at] [datetime] NOT NULL,
 CONSTRAINT [PK_collections] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


