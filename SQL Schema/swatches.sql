﻿USE [SwatchbookData]
GO

/****** Object:  Table [dbo].[swatches]    Script Date: 2/22/2021 12:26:55 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[swatches](
	[id] [varchar](50) NOT NULL,
	[name] [varchar](100) NOT NULL,
	[color] [varchar](50) NOT NULL,
	[uuid] [varchar](50) NOT NULL,
	[tags] [text] NULL,
	[author] [varchar](100) NULL,
	[supplier_name] [varchar](50) NOT NULL,
	[approval_status] [smallint] NOT NULL,
	[visibility] [int] NOT NULL,
	[organization_name] [varchar](50) NOT NULL,
	[ipid] [varchar](50) NULL,
	[price] [float] NOT NULL,
	[price_dimension_unit] [varchar](50) NULL,
	[updated_at] [datetime] NULL,
	[width] [float] NULL,
	[height] [float] NULL,
	[color_map] [varchar](100) NULL,
	[job_update_at] [datetime] NOT NULL,
 CONSTRAINT [PK_swatches] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[swatches] ADD  CONSTRAINT [DF_swatches_approval_status]  DEFAULT ((0)) FOR [approval_status]
GO

ALTER TABLE [dbo].[swatches] ADD  CONSTRAINT [DF_swatches_price]  DEFAULT ((0)) FOR [price]
GO

