﻿using JobClients;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SwatchbookDataJob
{
    class Program
    {
        private enum JobMode { Full, Latest };
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        static void Main(string[] args)
        {
            var apiKey = ConfigurationManager.AppSettings["access_token"];
            var dbConn = ConfigurationManager.AppSettings["db_connection"];
            var lastrun = HelperFns.GetLastRun();
            DateTime? since = null;

            var assetop = "updated";

            JobMode mode = JobMode.Latest;

            if (lastrun != null && lastrun != string.Empty)
            {
                log.Debug("last-run.txt found: " + lastrun);
                since = DateTime.Parse(lastrun);
            }
            else
            {
                log.Debug("last-run.txt not found.");
            }

            if (args.Length > 0 && args[0] == "-full")
            {
                mode = JobMode.Full;
                log.Debug("Full data set option recognized.");
                since = null;
            }
            else if (lastrun == null)
            {
                log.Debug("Defaulting to full asset import.");
                mode = JobMode.Full;
                since = null;
            }


            SwatchbookClient swatchClient = new SwatchbookClient(apiKey);
            SQLClient sqlClient = new SQLClient(dbConn);
            Task.Run(async () =>
            {
                List<JObject> items;
                log.Info("Syncing all swatches" + ((since != null) ? " since " + since.ToString() : ""));
                do
                {
                    items = await swatchClient.GetSwatches(since);
                    for(int i = 0; i < items.Count; i++)
                    {
                        var item = items[i];
                        items[i] = await swatchClient.GetSwatch(item["id"].ToString());
                        Thread.Sleep(100);
                    }
                    try
                    {
                        lastrun = sqlClient.WriteCollectionRecords(items, "SB.swatches", "updated_at.date");

                        if (DateTime.TryParse(lastrun, out DateTime latestedit))
                        {
                            log.Debug($"Wrote db for {items.Count} assets {assetop} at {latestedit.ToString("u").Replace(' ', 'T')}");
                            HelperFns.WriteLastRun(lastrun);
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                    Thread.Sleep(100);
                } while (swatchClient.HasMore);

                swatchClient.Reset();
                List<JObject> cols = new List<JObject>();
                do
                {
                    items = await swatchClient.GetCollections();
                    sqlClient.WriteCollectionRecords(items, "SB.collections", "updated_at.date");
                    cols.AddRange(items);
                } while (swatchClient.HasMore);

                foreach (var col in cols)
                {
                    log.Info($"Syncing collection {col["name"].ToString()} swatches" + ((since != null) ? " since " + since.ToString() : "") + ".");
                    swatchClient.Reset();
                    do
                    {
                        items = await swatchClient.GetCollectionSwatches(col["id"].ToString(), since);
                        if (items.Count > 0)
                        {
                            sqlClient.WriteCollectionRecords(items, "SB.collection_swatch", null, "collectionSwatchId");
                        }
                    } while (swatchClient.HasMore);
                }
            }).GetAwaiter().GetResult();

        }
    }
}
