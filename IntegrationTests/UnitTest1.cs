﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Threading.Tasks;
using JobClients;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json.Linq;
using Microsoft.CSharp;

namespace IntegrationTests
{
    [TestClass]
    public class UnitTest1
    {
        private string apiKey;
        private string dbConn;
        public UnitTest1()
        {
            apiKey = ConfigurationManager.AppSettings["access_token"];
            dbConn = ConfigurationManager.AppSettings["db_connection"];
        }
        [TestMethod]
        public void GetSomeCollections()
        {
            Task.Run(async () =>
            {
                SwatchbookClient swatchClient = new SwatchbookClient(apiKey);
                var res = await swatchClient.GetCollections();
                Console.WriteLine(res);
            }).GetAwaiter().GetResult();
        }
        [TestMethod]
        public void GetAllCollections()
        {
            Task.Run(async () =>
            {
                SwatchbookClient swatchClient = new SwatchbookClient(apiKey);
                List<JObject> items = new List<JObject>();
                while (swatchClient.HasMore)
                {
                    var res = await swatchClient.GetCollections();
                    items.AddRange(res);
                }
                SQLClient sqlClient = new SQLClient(dbConn);
                sqlClient.WriteCollectionRecords(items, "collections", null);
                Console.WriteLine(items.Count);
            }).GetAwaiter().GetResult();
        }

        [TestMethod]
        public void GetCollectionSwatches()
        {
            Task.Run(async () =>
            {
                SwatchbookClient swatchClient = new SwatchbookClient(apiKey);
                List<JObject> items = new List<JObject>();
                while (swatchClient.HasMore)
                {
                    var res = await swatchClient.GetCollectionSwatches("3c640c30-5044-11eb-818b-c9fcd9b73a49");
                    items.AddRange(res);
                }
                //SQLClient sqlClient = new SQLClient(dbConn);
                //sqlClient.WriteCollectionRecords(items, null);
                Console.WriteLine(items.Count);
                SQLClient sqlClient = new SQLClient(dbConn);
                sqlClient.WriteCollectionRecords(items, "swatches", null);
                sqlClient.WriteCollectionRecords(items, "collection_swatch", null);
            }).GetAwaiter().GetResult();
        }

        [TestMethod]
        public void ResolveField()
        {
            var jobj = JObject.Parse(File.ReadAllText("TestData/TestAssetFromKevin.txt"));
            string fn = "";
            string res = HelperFns.ResolveField2(jobj, $"prices.items[?(@.unit=='USD' && @.is_main_price==1)].dimension_unit", out fn);   
        }

        [TestMethod]
        public void ResolveField2()
        {
            var jobj = JObject.Parse(File.ReadAllText("TestData/BadAsset1.txt"));
            string fn = "";
            string res = HelperFns.ResolveField2(jobj, $"attributes[?(@.name=='IPID')].value", out fn);
        }

        [TestMethod]
        public void ResolveField3()
        {
            var jobj = JObject.Parse(File.ReadAllText("TestData/TestAssetFromKevin.txt"));
            string fn = "";
            string res = HelperFns.ResolveField2(jobj, $"attributes[?(@.name=='visualization')].children[?(@.name=='COLORWAY')].children[?(@.name=='color map')].children[?(@.name=='file')].swatchAttachment.name", out fn);
        }

        [TestMethod]
        public void WriteLastRunTime()
        {
            HelperFns.WriteLastRun(HelperFns.PrepSQLDate(DateTime.Now.ToString("u")));
        }

        [TestMethod]
        public void GetLastRunTime()
        {
            var lr = HelperFns.GetLastRun();
        }
    }
}
