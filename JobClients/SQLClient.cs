﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using Newtonsoft.Json.Linq;


namespace JobClients
{
    public class SQLClient
    {
        private SqlConnection conn;
        private Dictionary<string, Dictionary<string, string>> fmap;
        public SQLClient(string dbconn)
        {
            conn = new SqlConnection(dbconn);
            fmap = new Dictionary<string, Dictionary<string, string>>
            {
                {"SB.collections",
                    new Dictionary<string, string>{
                        { "id", "id" },
                        { "name", "name" },
                        { "updated_at.date@", "updated_at" }
                    }
                },
                {"SB.swatches",
                    new Dictionary<string, string>{
                        { "id", "id" },
                        { "name", "name" },
                        { "uuid", "uuid" },
                        { "color", "color" },
                        { "supplier.name", "supplier_name" },
                        { "owner.email", "author" },
                        { "progress#", "approval_status" },
                        { "tags", "tags" },
                        { "organization.name", "organization_name" },
                        { "visibility#", "visibility" },
                        { "attributes[?(@.name=='IPID')].value", "ipid" },
                        { "prices.items[?(@.unit=='USD' && @.is_main_price==1 && @.title=='sample')].amount#", "price" },
                        { "prices.items[?(@.unit=='USD' && @.is_main_price==1 && @.title=='sample')].dimension_unit", "price_dimension_unit" },
                        { "attributes[?(@.name=='visualization')].children[?(@.name=='COLORWAY')].children[?(@.name=='color map')].children[?(@.name=='file')].swatchAttachment.name", "color_map" },
                        { "width#", "width" },
                        { "height#", "height" },
                        { "updated_at.date@", "updated_at" }
                    }
                },
                {
                    "SB.collection_swatch", new Dictionary<string, string>
                    {
                        {"collectionSwatchId", "id" },
                        {"id", "swatch_id" },
                        {"collectionId", "collection_id" }
                    }
                }
            };
        }
        public string WriteCollectionRecords(List<JObject> assets, string DB_TABLE, string datekey, string pkey="id")
        {
            var ret = "";
            var schema = GetSchema(DB_TABLE);
            conn.Open();
            var cmd = new SqlCommand("", conn);
            int insertcount = 0;
            int updatecount = 0;
            Regex rexc = new Regex("[A-Z0-9_]", RegexOptions.IgnoreCase);

            foreach (var asset in assets)
            {
                var valuesstr = "";
                var updatestr = "";
                var id = asset[pkey].ToString();
                string sql = $"IF NOT EXISTS (SELECT * FROM {DB_TABLE} WHERE id='{id}') INSERT INTO {DB_TABLE} (";
                sql += string.Join(",", schema) + ") ";

                try
                {
                    foreach (var key in fmap[DB_TABLE].Keys)
                    {
                        dynamic target = asset;
                        var fn = "";

                        target = HelperFns.ResolveField2(target, key, out fn);

                        if (fn == "#")
                        {
                            if (target == null)
                            {
                                valuesstr += "0,";
                                updatestr += $"{fmap[DB_TABLE][key]}=0,";
                            }
                            else
                            {
                                valuesstr += HelperFns.PrepSQL(target) + ",";
                                updatestr += $"{fmap[DB_TABLE][key]}={HelperFns.PrepSQL(target)},";
                            }
                        }
                        else if (fn == "@")
                        {
                            if (target == null)
                            {
                                valuesstr += "'1970-01-01T00:00:00Z',";
                                updatestr += $"{fmap[DB_TABLE][key]}='1970-01-01T00:00:00Z',";
                            }
                            else
                            {
                                string dt = HelperFns.PrepSQLDate(target);
                                valuesstr += $"'{dt}',";
                                updatestr += $"{fmap[DB_TABLE][key]}='1970-01-01T00:00:00Z',";
                            }
                        }
                        else
                        {
                            if (target == null || (target.GetType() == typeof(JArray) && !((JArray)target).HasValues))
                            {
                                valuesstr += "'',";
                                updatestr += $"{fmap[DB_TABLE][key]}='',";
                            }
                            else
                            {
                                valuesstr += $"'{HelperFns.PrepSQL(target)}',";
                                updatestr += $"{fmap[DB_TABLE][key]}='{HelperFns.PrepSQL(target)}',";
                            }
                        }
                    }
                } catch(Exception e)
                {
                    Console.WriteLine("Could not parse asset json");
                    Console.WriteLine(asset);
                    continue;
                }

                var dtstr = DateTime.Now.ToString("u").Replace(' ', 'T');
                valuesstr += $"'{dtstr}'";
                updatestr += $"job_update_at='{dtstr}'";

                valuesstr = valuesstr.TrimEnd(new char[] { ',' });
                updatestr = updatestr.TrimEnd(new char[] { ',' });
                sql += $" VALUES ({valuesstr});";
                cmd.CommandText = sql.Replace('\n', ' ').Replace('\r', ' ');
                int insertval = 0;
                try
                {
                    insertval = cmd.ExecuteNonQuery();
                }
                catch (SqlException sx)
                {
                    Exception throwex = new Exception(cmd.CommandText, sx);
                    throw throwex;
                }

                if (insertval > 0)
                {
                    insertcount += insertval;
                }
                else
                {
                    cmd.CommandText = $"UPDATE {DB_TABLE} SET {updatestr} WHERE id='{id}';".Replace('\n', ' ').Replace('\r', ' ');
                    int updateval;
                    try
                    {
                        updateval = cmd.ExecuteNonQuery();
                    }
                    catch (SqlException sx)
                    {
                        Exception throwex = new Exception(cmd.CommandText, sx);
                        throw throwex;
                    }
                    updatecount++;
                }

                string datestr = HelperFns.ResolveField2(asset, datekey, out string _);
                if (datestr != null)
                {
                    if (ret == "" || DateTime.Parse(ret) < DateTime.Parse(datestr))
                        ret = HelperFns.PrepSQLDate(datestr);
                }
            }
            conn.Close();
            Console.WriteLine($"{insertcount}/{assets.Count} inserted, {updatecount}/{assets.Count} updated");
            return ret;
        }
        private List<string> GetSchema(string table)
        {
            var fldnames = new List<string>(fmap[table].Values);
            fldnames.Add("job_update_at");
            return fldnames;
        }


    }
}
