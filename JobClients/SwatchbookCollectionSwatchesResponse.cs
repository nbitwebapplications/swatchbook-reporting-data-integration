﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JobClients
{
    public class SwatchbookCollectionSwatchesResponse : SwatchbookSwatchesResponse
    {
        private string collection;

        public new List<JObject> Items
        {
            get
            {
                List<JObject> ret = new List<JObject>();
                JArray items = (JArray)baseObj["items"];
                foreach (JObject item in items)
                {
                    if (!item.ContainsKey("collectionId"))
                    {
                        item.Add("collectionId", collection);
                        item.Add("collectionSwatchId", collection + "_" + item["id"]);
                    }
                    ret.Add(item);
                }
                return ret;
            }
        }
        public SwatchbookCollectionSwatchesResponse(string res, string collectionId) : base(res)
        {
            collection = collectionId;
        }
    }
}
