﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JobClients
{
    class SwatchbookCollectionResponse
    {
        private JObject baseObj;
        public bool HasMoreItems
        {
            get
            {
                return (bool)baseObj["hasMorePages"];
            }
        }
        public int CurrentPage
        {
            get
            {
                return (int)baseObj["currentPage"];
            }
        }
        public List<JObject> Items
        {
            get
            {
                List<JObject> ret = new List<JObject>();
                JArray items = (JArray)baseObj["items"];
                foreach(JObject item in items)
                {
                    ret.Add(item);
                }
                return ret;
            }
        }
        public SwatchbookCollectionResponse(string res)
        {
            baseObj = JObject.Parse(res);
        }
    }
}
