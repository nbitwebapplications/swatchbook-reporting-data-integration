﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace JobClients
{
    public class SwatchbookClient
    {
        private const string URL_SwatchbookAPI = "https://nb.swatchbook.us/api/v1/";
        private HttpClient client;
        private int itemcount = 0;
        private int progress = 0;
        private int page = 1;
        private bool hasMore = true;

        public int ItemCount
        {
            get
            {
                return itemcount;
            }
        }

        public bool HasMore
        {
            get
            {
                return hasMore;
            }
        }

        public SwatchbookClient(string accesstoken)
        {
            client = new HttpClient();
            client.BaseAddress = new Uri(URL_SwatchbookAPI);

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "Bearer " + accesstoken);
        }

        public void Reset()
        {
            page = 1;
            hasMore = true;
            itemcount = 0;
            progress = 0;
        }

        public async Task<List<JObject>> GetCollections()
        {
            hasMore = false;
            List<JObject> ret = new List<JObject>();
            SwatchbookCollectionResponse sirres;
            var res = await client.GetAsync($"collection?page={page}");
            var resstr = await res.Content.ReadAsStringAsync();
            sirres = new SwatchbookCollectionResponse(resstr);
            itemcount += sirres.Items.Count;
            hasMore = sirres.HasMoreItems;
            ret.AddRange(sirres.Items);
            page++;
            return ret;
        }

        public async Task<List<JObject>> GetCollectionSwatches(string collectionId, DateTime? since = null)
        {
            hasMore = false;
            List<JObject> ret = new List<JObject>();
            SwatchbookCollectionSwatchesResponse sirres;
            HttpResponseMessage res;
            if(since == null)
            {
                res = await client.GetAsync($"swatch?collection_id={collectionId}&orderColumn=updated_at&orderType=asc&page={page}");
            }
            else
            {
                res = await client.GetAsync($"swatch?collection_id={collectionId}&orderColumn=updated_at&orderType=desc&page={page}");
            }
            var resstr = await res.Content.ReadAsStringAsync();
            sirres = new SwatchbookCollectionSwatchesResponse(resstr, collectionId);
            if(since == null)
            {
                itemcount += sirres.Items.Count;
                hasMore = sirres.HasMoreItems;
                ret.AddRange(sirres.Items);
                page++;
            }
            else
            {
                foreach(var sr in sirres.Items)
                {
                    dynamic updatedt = HelperFns.ResolveField2(sr, "updated_at.date@", out _);
                    DateTime update = DateTime.Parse((string)updatedt);
                    if(update > since)
                    {
                        ret.Add(sr);
                        itemcount++;
                    }
                    else
                    {
                        hasMore = false;
                        return ret;
                    }
                }
                page++;
            }
            return ret;
        }

        public async Task<JObject> GetSwatch(string id)
        {
            var res = await client.GetAsync($"swatch/{id}");
            var resstr = await res.Content.ReadAsStringAsync();
            return JObject.Parse(resstr);
        }

        public async Task<List<JObject>> GetSwatches(DateTime? since)
        {
            hasMore = false;
            List<JObject> ret = new List<JObject>();
            SwatchbookSwatchesResponse sirres;
            HttpResponseMessage res;
            if (since == null)
            {
                res = await client.GetAsync($"swatch?orderColumn=updated_at&orderType=asc&page={page}");
            }
            else
            {
                res = await client.GetAsync($"swatch?orderColumn=updated_at&orderType=desc&page={page}");
            }
            var resstr = await res.Content.ReadAsStringAsync();
            sirres = new SwatchbookSwatchesResponse(resstr);
            if(since == null)
            {
                itemcount += sirres.Items.Count;
                hasMore = sirres.HasMoreItems;
                ret.AddRange(sirres.Items);
                page++;
            }
            else
            {
                foreach (var sr in sirres.Items)
                {
                    dynamic updatedt = HelperFns.ResolveField2(sr, "updated_at.date@", out _);
                    DateTime update = DateTime.Parse((string)updatedt);
                    if (update > since)
                    {
                        ret.Add(sr);
                        itemcount++;
                    }
                    else
                    {
                        hasMore = false;
                        return ret;
                    }
                }
            }
            return ret;
        }

        public enum QueryType { Latest, AllTime };
    }
}
