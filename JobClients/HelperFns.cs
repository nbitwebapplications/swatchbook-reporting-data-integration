﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace JobClients
{
    public class HelperFns
    {
        public static string GetLastRun()
        {
            var exeloc = AppDomain.CurrentDomain.BaseDirectory;
            var lastrunfile = Path.Combine(exeloc, "last-run.txt");
            if (File.Exists(lastrunfile))
            {
                string lastrun = File.ReadAllText(lastrunfile).Split(Environment.NewLine.ToCharArray())[0];
                return lastrun;
            }
            return null;
        }
        public static void WriteLastRun(string lastrun)
        {
            var exeloc = AppDomain.CurrentDomain.BaseDirectory; // https://stackoverflow.com/a/8747661
            var lastrunfile = Path.Combine(exeloc, "last-run.txt");
            StreamWriter sw = new StreamWriter(lastrunfile, false);
            sw.WriteLine(lastrun);
            sw.Flush();
            sw.Close();
        }
        public static dynamic ResolveField2(dynamic target, string key, out string fn)
        {

            string ret = string.Empty;
            fn = "";

            if (key == null)
                return null;

            Regex rexfn = new Regex("[@#$]$", RegexOptions.Singleline);
            var ms = rexfn.Matches(key);
            if(ms != null && ms.Count > 0)
            {
                fn = key.Substring(ms[0].Index);
                key = key.Remove(ms[0].Index);
            }
            JToken jret;
            try
            {
                return ((JObject)target).SelectToken("$." + key);
            }
            catch (Newtonsoft.Json.JsonException je)
            {
                return ((JObject)target).SelectTokens("$." + key).FirstOrDefault();
            }
        }
        public static dynamic ResolveField(dynamic target, string key, out string fn)
        {
            string ret = string.Empty;
            Regex rexc = new Regex("[A-Z0-9_]", RegexOptions.IgnoreCase);
            var part = "";
            fn = "";
            for (var c = 0; c < key.Length; c++)
            {
                if (rexc.IsMatch("" + key[c]))
                {
                    part += key[c];
                }
                else if (key[c] == '.')
                {

                    if (((JObject)target).ContainsKey(part) && target[part].GetType() != typeof(JArray))
                    {
                        target = (JObject)target[part];
                        part = "";
                    }
                    else
                    {
                        target = null;
                        break;
                    }
                }
                else if (key[c] == '[')
                {
                    target = (JArray)target[part];
                    part = "";
                    if (target != null && ((JArray)target).HasValues)
                    {
                        c += 1;
                        for (; c < key.Length && key[c] != ']'; c++)
                            part += key[c];

                        int pint;
                        if (Int32.TryParse(part, out pint))
                        {
                            target = target[pint];
                            part = "";
                        }
                        else if(part.Contains("="))
                        {

                        }
                    }
                    else
                        break;
                }
                else if (key[c] == '#' || key[c] == '$' || key[c] == '@')
                {
                    fn = key[c] + "";
                }
            }
            if (part != "" && target != null)
            {
                if (((JObject)target).ContainsKey(part))
                {
                    target = target[part];
                }
                else
                {
                    target = null;
                }
            }

            return target;
        }
        public static string PrepSQLDate(dynamic target)
        {
            string ret = target + "";
            ret = ret.Replace(' ', 'T');
            ret = ret.Split(new char[] { '.' })[0];
            ret += "Z";
            return ret;
        }

        public static string PrepSQL(dynamic target)
        {
            string ret = string.Empty;

            if (target.GetType() == typeof(JArray))
            {
                ret = string.Join(", ", target);
            }
            else
                ret = target.ToString();

            return ret.Replace("'", "''").Replace('\n', ' ').Replace('\r', ' ');
        }
    }
}
